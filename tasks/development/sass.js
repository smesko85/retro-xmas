/* global gulp, config, plumber, notify */
'use strict';

var sass = require('gulp-sass');
var connect = require('gulp-connect');
var scsslint = require('gulp-scss-lint');
var autoprefixer = require('gulp-autoprefixer');

var sassPath = config.srcPath + config.sassDir;
var cssBuildPath = config.buildPath + config.cssDir;

gulp.task('scss-lint', function() {
  return gulp.src(config.srcPath + config.sassDir + '**/*.scss')
    .pipe(scsslint({
      config: '.scss-lint.yml'
    }));
});

gulp.task('sass', function() {
  return gulp.src(sassPath + '**/*.scss')
    .pipe(plumber({
      errorHandler: notify.onError({
        title: 'SCSS',
        message: function(err) {
          return 'Error: ' + err.message;
        }
      })
    }))
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(autoprefixer({
        browsers: ['last 2 versions', 'ie >= 10']
    }))
    .pipe(gulp.dest(cssBuildPath))
    .pipe(connect.reload());
});
