/* global gulp, config, del, plumber */
'use strict';

var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');
var dedupe = require('gulp-dedupe');
var eslint = require('gulp-eslint');

gulp.task('eslint', function () {
  return gulp.src(config.srcPath + config.jsDir + '**/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('clean:js', function(cb) {
  return del([
    config.buildPath + config.jsDir + '**/*.js'
  ], cb);
});

gulp.task('usemin', ['default', 'clean:js'], function() {
  return gulp.src(config.buildPath + '*.html')
    .pipe(plumber())
    .pipe(dedupe({same: false}))
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('!*.html', rev()))
    .pipe(revReplace())
    .pipe(gulp.dest(config.buildPath));
});

gulp.task('js-lint', ['eslint']);
