/* global gulp, config, del, plumber, notify */
'use strict';

var prettify = require('gulp-prettify');
var connect = require('gulp-connect');
var fs = require('graceful-fs');
var nunjucksRender = require('gulp-nunjucks-render');

function getHtmlFiles() {
  return fs.readdirSync(config.buildPath).filter(function(file) {
    return file !== config.indexPage && file.indexOf('.html') !== -1;
  });
}

gulp.task('clean:html', function(cb) {
  return del([
    config.buildPath + '*.html'
  ], cb);
});

gulp.task('nunjucks:compile', ['clean:html'], function() {
  return gulp.src(config.srcPath + config.markupDir + '*.html')
    .pipe(plumber({
      errorHandler: notify.onError({
        title: 'HTML - Nunjucks',
        message: function(err) {
          return 'Error: ' + err.message;
        }
      })
    }))
    .pipe(nunjucksRender({
      path: config.srcPath + config.markupDir
    }))
    .pipe(gulp.dest(config.buildPath));
});

gulp.task('pages-list', ['nunjucks:compile'], function() {
  return gulp.src(config.srcPath + config.markupDir + config.indexPage)
    .pipe(plumber())
    .pipe(nunjucksRender({
      path: config.srcPath + config.markupDir,
      data: {
        files: getHtmlFiles()
      }
    }))
    .pipe(gulp.dest(config.buildPath))
    .pipe(connect.reload());
});

gulp.task('prettify', ['pages-list'], function() {
  return gulp.src(config.buildPath + '*.html')
    .pipe(plumber())
    .pipe(prettify({
      'indent_size': 2,
      'max_preserve_newlines': 2
    }))
    .pipe(gulp.dest(config.buildPath));
});

gulp.task('nunjucks', ['pages-list']);
