module.exports = {
  indexPage: 'index.html',
  srcPath: 'frontend/',
  buildPath: 'public/',
  jsDir: 'js/',
  sassDir: 'scss/',
  cssDir: 'css/',
  imgDir: 'images/',
  iconsDir: 'icons/',
  markupDir: 'markup/',
  fontsDir: 'fonts/',
  faviconDir: 'favicon/',
  useLiveReload: true
};
