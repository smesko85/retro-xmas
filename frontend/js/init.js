/* globals $, svg4everybody */

/**
 * 1. As of v2.0.0, you must the manually call svg4everybody().
 *    If you are using an AMD/CommonJS dependency loader then you may
 *    call it within the callback closure.
 */

var SCREEN_WIDTH = 80;
var SCREEN_HEIGHT = 50;
var $pattern = {};
var pattern = '';

function generatePattern() {
  var char = ''.split('');
  var size = SCREEN_WIDTH * SCREEN_HEIGHT;
  var ptrn = '';

  for (var i = 0; i < size; i++) {
    ptrn += char[ Math.floor(Math.random() * char.length) ];
  }

  return ptrn;
}

function breakLines(string) {
  var str = string[0];
  var len = string.length;
  for (var i = 1; i < len; i++) {
    str += string[i];
    if (i % SCREEN_WIDTH > SCREEN_WIDTH - 2) str += '\r\n';
  }

  return str;
}

function draw() {
  // $pattern.data = breakLines(pattern);
  $pattern.data = breakLines(generatePattern());
}

function update() {
  pattern = pattern.slice(-1) + pattern.slice(0, -1);
}

function loop() {
  update();
  draw();
  setTimeout(loop, 250);
}

// ON DOCUMENT READY
$(document).ready(function() {
  'use strict';

  svg4everybody(); /* [1] */

  pattern = generatePattern();
  $pattern = document.createTextNode(breakLines(pattern));
  document.querySelector('.js-pattern').appendChild($pattern);
  loop();
});

// WINDOW ONLOAD
$(window).load(function() {
  'use strict';

  $(window).on('resize', function() {

  }).trigger('resize');
});
